package com.twuc.simplewebapplication.contract;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CreateProductRequest {

    @NotNull
    @Size(min = 1, max = 15)
    private String code;
    @NotNull
    @Size(min = 1, max = 70)
    private String name;
    @NotNull
    @Size(min = 1, max = 50)
    private String productLine;
    @NotNull
    @Size(min = 1, max = 10)
    private String scale;
    @NotNull
    @Size(min = 1, max = 50)
    private String vendor;
    @NotNull
    private String description;
    @NotNull
    private Short quantityInStock;
    @NotNull
    @Digits(integer = 8, fraction = 2)
    private BigDecimal buyPrice;
    @NotNull
    @Digits(integer = 8, fraction = 2)
    @JsonProperty
    private BigDecimal MSRP;

    private CreateProductRequest() {
    }

    public CreateProductRequest(
            @NotNull @Size(min = 1, max = 15) String code,
            @NotNull @Size(min = 1, max = 70) String name,
            @NotNull @Size(min = 1, max = 50) String productLine,
            @NotNull @Size(min = 1, max = 10) String scale,
            @NotNull @Size(min = 1, max = 50) String vendor,
            @NotNull String description,
            @NotNull Short quantityInStock,
            @NotNull @Digits(integer = 8, fraction = 2) BigDecimal buyPrice,
            @NotNull @Digits(integer = 8, fraction = 2) BigDecimal MSRP) {
        this.code = code;
        this.name = name;
        this.productLine = productLine;
        this.scale = scale;
        this.vendor = vendor;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.MSRP = MSRP;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    @JsonIgnore
    public BigDecimal getMSRP() {
        return MSRP;
    }
}