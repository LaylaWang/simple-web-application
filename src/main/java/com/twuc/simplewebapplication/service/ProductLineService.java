package com.twuc.simplewebapplication.service;

import com.twuc.simplewebapplication.contract.ProductLineResourceAssembler;
import com.twuc.simplewebapplication.dao.ProductLineRepository;
import com.twuc.simplewebapplication.domian.ProductLine;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ProductLineService {

    private ProductLineRepository repository;
    private ProductLineResourceAssembler assembler;

    public ProductLineService(ProductLineRepository repository, ProductLineResourceAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    public List<Resource<ProductLine>> getAll(Integer page, Integer size, String orderBy, String direction) {
        String defaultOrderBy = (orderBy == null) ? "name" : orderBy;
        Sort.Direction defaultDirection = (direction == null) ? Sort.Direction.ASC : Sort.Direction.DESC;
        Sort sort = new Sort(defaultDirection, defaultOrderBy);

        if (page != null && size != null) {
            return this.findAll(page, size, sort);
        } else {
            return this.findAll(sort);
        }
    }

    private List<Resource<ProductLine>> findAll(Sort sort) {
        Stream<ProductLine> stream = repository.findAll(sort).stream();
        return this.productLineStreamToResourceList(stream);
    }

    private List<Resource<ProductLine>> findAll(Integer page, Integer size, Sort sort) {
        Stream<ProductLine> stream = repository.findAll(PageRequest.of(page, size, sort)).stream();
        return this.productLineStreamToResourceList(stream);
    }

    private List<Resource<ProductLine>> productLineStreamToResourceList(Stream<ProductLine> stream) {
        return stream.map(productLine -> assembler.toResource(productLine))
                .collect(Collectors.toList());
    }

    public ProductLine getOne(String name) {
        return repository.findById(name).orElseThrow(NoSuchElementException::new);
    }

    public void create(ProductLine productLine) {
        repository.save(productLine);
    }
}
